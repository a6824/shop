// prérequis
const stripe = require('stripe')(process.env.STRIPE_SK ||'sk_test_VePHdqKTYQjKNInc7u56JBrQ')
const express = require('express')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const http = require("http")
const cors = require('cors')
const XMLHttpRequest = require('xhr2');
require("dotenv").config()

//variables nécessaire
const app = express()
const port = 3000

// set view engine to ejs
app.set('view engine', 'ejs')
app.use(express.static("public"))
// enabling cors request
app.use(cors())
// parse application/json
app.use(bodyParser.json())
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({
    extended: true
}))
// parse cookie
app.use(cookieParser())

//default routes de test
app.get("/api/readinnes", function (req, res) {
    res.json("hello world")
})

let nextCommandeId = 0;
let commandeBuffer = [];

app.get("/", function(req, res) {
    let xhr = new XMLHttpRequest();
    xhr.open("GET", "https://configoffre.legauf.fr/api/produits")
    xhr.onreadystatechange = function() {
        if(xhr.status === 200 && xhr.readyState === 4) {
            res.render("pages/shop", {produits: JSON.parse(xhr.response)})
        }
    }
    xhr.send()
})

app.get("/shop/produit/:id", function(req, res) {
    let upsell = [];
    let xhr = new XMLHttpRequest();
    xhr.open("GET", "https://configoffre.legauf.fr/api/produits/" + req.params.id)
    xhr.onreadystatechange = async () => {
        if(xhr.status === 200 && xhr.readyState === 4) {
            let y = 0;
            let produit = JSON.parse(xhr.response)
            let s = ".*" + produit.nom.match(/^[a-zA-Z]+/)[0] + ".*"
            let re = new RegExp(s, "g");
            JSON.parse(xhr3.response).forEach(element => {
                if (y <= 3) {
                    if (element.nom.match(re) && element.id != produit.id) {
                        upsell.push(element)
                        y++
                    }
                }
            })
            res.render("pages/produit", {produit: produit, upsell: upsell})
        }
    }
    let xhr3 = new XMLHttpRequest();
    xhr3.open("GET", "https://configoffre.legauf.fr/api/produits/")
    xhr3.onreadystatechange = async () => {
        if(xhr3.status === 200 && xhr3.readyState === 4) {
            xhr.send()
        }
    }
    xhr3.send()
})

app.post('/shop/payer/:id', async (req, res) => {
    let xhr = new XMLHttpRequest();
    xhr.open("GET", "https://configoffre.legauf.fr/api/produits/" + req.params.id)
    xhr.onreadystatechange = async () => {
        if(xhr.status === 200 && xhr.readyState === 4) {
            const session = await stripe.checkout.sessions.create({
                line_items: [
                    {
                        // Provide the exact Price ID (for example, pr_1234) of the product you want to sell
                        price: JSON.parse(xhr.response).prix,
                        quantity: req.body.quantite,
                    },
                ],
                mode: 'payment',
                success_url: `https://shop.legauf.fr/shop/success`,
                cancel_url: `https://shop.legauf.fr/shop/cancel`,
            })
            let commande = {
                id: nextCommandeId,
                idPayment: session.id,
                idObjet: req.params.id,
                prix: JSON.parse(xhr.response).prix,
                quantite: req.body.quantite,
                emailClient: req.body.email,
                status: "en_attente",
                type: "envoi"
            }
            commandeBuffer.push(commande)
            nextCommandeId++
            res.redirect(303, session.url);
        }
    }
    xhr.send()
});

app.get("/shop/success", function(req, res) {
    let xhr = new XMLHttpRequest();
    xhr.open("POST", "https://configoffre.legauf.fr/commandes")
    xhr.setRequestHeader("content-type", "application/json")
    xhr.onreadystatechange = function() {
        if(xhr.status === 200 && xhr.readyState === 4) {
            commandeBuffer = commandeBuffer.splice(idCommande, 1)
            res.render("pages/success")
        }
    }
    let idCommande = -1;
    for(let i = 0; commandeBuffer.length; i++) {
        if(commandeBuffer[i].idPayment === req.body.id) {
            idCommande = i;
        }
    }
    xhr.send(commandeBuffer[idCommande])
})

app.get("/shop/cancel", function(req, res) {
    let idCommande = -1;
    for(let i = 0; commandeBuffer.length; i++) {
        if(commandeBuffer[i].idPayment === req.body.id) {
            idCommande = i;
        }
    }
    commandeBuffer = commandeBuffer.splice(idCommande, 1)
    res.render("pages/success")
})

//lancement serveur
console.log(`listening on port ${port}`)
http.createServer(app).listen(port)
